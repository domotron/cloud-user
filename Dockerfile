FROM alpine:3.14
RUN apk --no-cache add \
    composer \
    php7
WORKDIR /app
COPY . .
RUN composer --no-cache install
ENTRYPOINT ["./docker-entrypoint.sh"]
