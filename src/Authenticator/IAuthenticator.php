<?php

namespace DomotronCloudUser\Authenticator;

interface IAuthenticator
{
    /**
     * Authenticate user against cloud
     * @param string $username
     * @param string|null $password
     * @return array [$sessionId, $expire]
     */
    public function authenticate($username, $password = null);

    /**
     * Invalidate user authentication against cloud
     * @param string|null $sessionId
     * @return bool
     */
    public function invalidate($sessionId = null);

    /**
     * Extend user authentication against cloud
     * @param string $sessionId
     * @return int expiration date
     */
    public function extend($sessionId);
}
