<?php

namespace DomotronCloudUser\IdentityProvider\Cache;

interface ICache
{
    /**
     * @return array|null
     */
    public function get();

    /**
     * @param array $data
     * @return void
     */
    public function set(array $data);

    /**
     * Clear all data from cache
     * @return bool
     */
    public function clear();
}
