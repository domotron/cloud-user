<?php

namespace DomotronCloudUser\IdentityProvider\Cache;

use DateTime;

class SessionCache implements ICache
{
    /** @var string Session section name */
    private $sessionSection = 'domoCloudToken';

    /** @var string */
    private $validationInterval;

    /** @var int */
    private $now;

    /**
     * @param string $validationInterval
     */
    public function __construct($validationInterval = '5 minutes')
    {
        $this->validationInterval = $validationInterval;
    }

    /**
     * @return array|null
     */
    public function get()
    {
        if (isset($_SESSION[$this->sessionSection]['userData']) && $_SESSION[$this->sessionSection]['userData']['expiration'] >= $this->getNowTimestamp()) {
            return $_SESSION[$this->sessionSection]['userData']['data'];
        }
        return null;
    }

    /**
     * @param array $data
     */
    public function set(array $data)
    {
        $_SESSION[$this->sessionSection]['userData'] = [
            'data' => $data,
            'expiration' => (new DateTime('+' . $this->validationInterval))->getTimestamp()
        ];
    }

    /**
     * Clear all data from cache
     * @return bool
     */
    public function clear()
    {
        unset($_SESSION[$this->sessionSection]);
        return !array_key_exists($this->sessionSection, $_SESSION);
    }

    /**
     * @return int
     */
    private function getNowTimestamp()
    {
        if ($this->now === null) {
            $this->now = (new DateTime())->getTimestamp();
        }
        return $this->now;
    }
}
