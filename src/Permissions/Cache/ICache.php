<?php

namespace DomotronCloudUser\Permissions\Cache;

use DomotronCloudUser\Permissions\Permissions;

interface ICache
{
    /**
     * Fetch permissions from cache
     * @param string $key
     * @return Permissions|null
     */
    public function get($key);

    /**
     * Fetch all cached permissions
     * In format:
     * <code>
     * [
     *     key1 => [
     *         'permissions' => new Permissions(...),
     *         'expire' => timestamp
     *     ],
     *     key2 => [
     *         'permissions' => new Permissions(...),
     *         'expire' => timestamp
     *     ]
     * ]
     * </code>
     * @return array
     */
    public function getAll();

    /**
     * Set permissions to cache
     * @param string $key
     * @param Permissions $permissions
     */
    public function set($key, Permissions $permissions);

    /**
     * Clear all data from cache
     * @return bool
     */
    public function clear();
}
