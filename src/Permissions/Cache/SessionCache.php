<?php

namespace DomotronCloudUser\Permissions\Cache;

use DomotronCloudUser\Permissions\Permissions;
use DateTime;

class SessionCache implements ICache
{
    /** @var string */
    private $expiration;

    /** @var string */
    private $sessionSection;

    /**
     * @param string $expiration
     * @param string $sessionSection
     */
    public function __construct($expiration = '5 minutes', $sessionSection = 'cloudUserPermissions')
    {
        $this->expiration = $expiration;
        $this->sessionSection = $sessionSection;
    }

    /**
     * Fetch permissions from cache
     * @param string $key
     * @return Permissions|null
     */
    public function get($key)
    {
        $now = (new DateTime())->getTimestamp();
        if (isset($_SESSION[$this->sessionSection][$key]) && $_SESSION[$this->sessionSection][$key]['expire'] < $now) {
            unset($_SESSION[$this->sessionSection][$key]);
        }

        return isset($_SESSION[$this->sessionSection][$key]['permissions']) ? $_SESSION[$this->sessionSection][$key]['permissions'] : null;
    }

    /**
     * Fetch all cached permissions
     * @return array
     */
    public function getAll()
    {
        return isset($_SESSION[$this->sessionSection]) ? $_SESSION[$this->sessionSection] : [];
    }

    /**
     * Set permissions to cache
     * @param string $key
     * @param Permissions $permissions
     */
    public function set($key, Permissions $permissions)
    {
        $this->clean();
        $_SESSION[$this->sessionSection][$key] = [
            'permissions' => $permissions,
            'expire' => (new DateTime('+' . $this->expiration))->getTimestamp()
        ];
    }

    /**
     * Clear all data from cache
     * @return bool
     */
    public function clear()
    {
        unset($_SESSION[$this->sessionSection]);
        return !array_key_exists($this->sessionSection, $_SESSION);
    }

    /**
     * Clean expired permissions
     */
    private function clean()
    {
        $now = (new DateTime())->getTimestamp();
        foreach ($this->getAll() as $key => $permissions) {
            if ($permissions['expire'] < $now) {
                unset($_SESSION[$this->sessionSection][$key]);
            }
        }
    }
}
