<?php

namespace DomotronCloudUser\Permissions;

class Permissions
{
    /** @var array */
    private $permissions;

    /**
     * @param array $permissions
     */
    public function __construct(array $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * Check if user has permission to resource + privilege
     * @param string $resource
     * @param string $privilege
     * @return bool
     */
    public function isAllowed($resource, $privilege)
    {
        if (!isset($this->permissions[$resource])) {
            return false;
        }

        if (!$privilege || in_array($privilege, $this->permissions[$resource])) {
            return true;
        }

        return false;
    }
}
