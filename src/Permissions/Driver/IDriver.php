<?php

namespace DomotronCloudUser\Permissions\Driver;

interface IDriver
{
    /**
     * @param int $userId
     * @param array $keys
     * @return array
     */
    public function getPermissions($userId, array $keys);
}
