<?php

namespace DomotronCloudUser\Permissions\Driver;

use DomotronCloudClient\CloudClient;

class ApiDriver implements IDriver
{
    /** @var CloudClient */
    private $cloudClient;

    /**
     * @param CloudClient $cloudClient
     */
    public function __construct(CloudClient $cloudClient)
    {
        $this->cloudClient = $cloudClient;
    }

    /**
     * @param int $userId
     * @param array $keys
     * @return array
     */
    public function getPermissions($userId, array $keys)
    {
        return $this->cloudClient->userEndpoint()->getPermissions($userId, $keys);
    }
}
