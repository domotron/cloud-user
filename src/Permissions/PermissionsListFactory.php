<?php

namespace DomotronCloudUser\Permissions;

use DomotronCloudUser\Permissions\Cache\ICache;
use DomotronCloudUser\Permissions\Driver\IDriver;

class PermissionsListFactory
{
    /** @var IDriver */
    private $driver;

    /** @var ICache */
    private $cache;

    /** @var array */
    private $keyAliases = [];

    /**
     * @param IDriver $driver
     * @param ICache $cache
     */
    public function __construct(IDriver $driver, ICache $cache)
    {
        $this->driver = $driver;
        $this->cache = $cache;
    }

    /**
     * Register key alias for later easy use
     * eg. control-IDENTIFIER can be registered as control alias for easy use
     * @param string $alias
     * @param string $key
     */
    public function registerKeyAlias($alias, $key)
    {
        $this->keyAliases[$alias] = $key;
    }

    /**
     * @param int $cloudUserId
     * @return PermissionsList
     */
    public function create($cloudUserId)
    {
        return new PermissionsList($this->driver, $this->cache, $this->keyAliases, $cloudUserId);
    }

    /**
     * Clear permissions cache
     * @return bool
     */
    public function clearCache()
    {
        return $this->cache->clear();
    }
}
