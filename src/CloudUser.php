<?php

namespace DomotronCloudUser;

use DomotronCloudUser\Authenticator\IAuthenticator;
use DomotronCloudUser\IdentityProvider\IIdentityProvider;
use DomotronCloudUser\Permissions\Permissions;
use DomotronCloudUser\Permissions\PermissionsList;
use DomotronCloudUser\Permissions\PermissionsListFactory;

class CloudUser
{
    /** @var Identity|null */
    protected $identity = null;

    /** @var IIdentityProvider */
    protected $identityProvider;

    /** @var IAuthenticator */
    private $authenticator;

    /** @var PermissionsListFactory */
    protected $permissionsListFactory;

    /** @var PermissionsList */
    protected $permissionsList;

    /** @var string */
    protected $cloudUrl;

    /** @var string */
    protected $cookieName;

    /** @var bool */
    protected $active;

    /** @var bool */
    protected $initialized = false;

    /**
     * @param IIdentityProvider $identityProvider
     * @param IAuthenticator $authenticator
     * @param PermissionsListFactory $permissionsListFactory
     * @param string $cloudUrl
     * @param string $cookieName
     * @param bool $active
     */
    public function __construct(
        IIdentityProvider $identityProvider,
        IAuthenticator $authenticator,
        PermissionsListFactory $permissionsListFactory,
        $cloudUrl,
        $cookieName = 'domotron-gw',
        $active = true
    ) {
        $this->identityProvider = $identityProvider;
        $this->authenticator = $authenticator;
        $this->permissionsListFactory = $permissionsListFactory;
        $this->cloudUrl = rtrim($cloudUrl, '/');
        $this->cookieName = $cookieName;
        $this->active = $active;

        $this->identity = new Identity();
    }

    /**
     * Check if cloud user is active
     * Non active cloud user will never be initialized
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Check if cloud user is initialized
     * @return bool
     */
    public function isInitialized()
    {
        return (bool) $this->initialized;
    }

    /**
     * Check if cloud user is logged in
     * @return bool
     */
    public function isLoggedIn()
    {
        $this->init();
        return !(bool) $this->identity->isEmpty();
    }

    /**
     * Returns all user data
     * @return Identity
     */
    public function getIdentity()
    {
        $this->init();
        return $this->identity;
    }

    /**
     * @param string $key
     * @return Permissions|null
     */
    public function permissions($key)
    {
        $this->init();
        if ($this->identity->isEmpty()) {
            return null;
        }

        if (!$this->permissionsList) {
            $this->permissionsList = $this->permissionsListFactory->create($this->identity->getId());
        }

        return $this->permissionsList->get($key);
    }

    /**
     * Shorthand for $cloudUser->permissions($key)->isAllowed($resource, $privilege)
     * @param string $key
     * @param string $resource
     * @param string $privilege
     * @param boolean $inactiveDefault  What to return if cloud user is not active
     * @return bool
     */
    public function permissionsIsAllowed($key, $resource, $privilege, $inactiveDefault = true)
    {
        if (!$this->isActive()) {
            return (int) $inactiveDefault;
        }

        $permissions = $this->permissions($key);
        if (!$permissions) {
            return false;
        }

        return $permissions->isAllowed($resource, $privilege);
    }

    /**
     * @return PermissionsList|null
     */
    public function getAllPermissions()
    {
        $this->init();
        if ($this->identity->isEmpty()) {
            return null;
        }

        if (!$this->permissionsList) {
            $this->permissionsList = $this->permissionsListFactory->create($this->identity->getId());
        }

        return $this->permissionsList;
    }

    /**
     * Generate login url
     * @param string $portal
     * @param string $secret
     * @param string $redirectUri
     * @return string
     */
    public function getLoginUrl($portal = null, $secret = null, $redirectUri = null)
    {
        if ($portal && $secret && $redirectUri) {
            return $this->cloudUrl . '/?' . http_build_query([
                    'sso_portal_id' => $portal,
                    'sso_portal_secret' => $secret,
                    'sso_redirect_uri' => $redirectUri
                ]);
        }

        return $this->cloudUrl;
    }

    /**
     * @return string
     */
    public function getLogoutUrl()
    {
        return $this->cloudUrl . '/sign/out';
    }

    /**
     * Initialize cloud user data
     */
    private function init()
    {
        if ($this->initialized) {
            return;
        }

        if (!$this->active) {
            $this->identity = new Identity();
            return;
        }

        $this->identity = $this->identityProvider->getIdentity($this->cookieName);

        if ($this->identity->isFresh()) {
            $sessionId = filter_input(INPUT_COOKIE, $this->cookieName);
            $expire = $this->authenticator->extend($sessionId);
            $this->setCookie($this->cookieName, $sessionId, $expire);
        }

        $this->initialized = true;
    }

    /**
     * Login user - only authenticate
     * @param string $username
     * @param string|null $password
     * @return bool
     */
    public function login($username, $password = null)
    {
        if (!$this->logout()) {
            return false;
        }

        list($sessionId, $expire) = $this->authenticator->authenticate($username, $password);

        if (!$sessionId) {
            return false;
        }

        return $this->setCookie($this->cookieName, $sessionId, $expire);
    }

    /**
     * Logout user
     * @return bool
     */
    public function logout()
    {
        // Clear old user data (identity and permissions) from cache
        if (!$this->identityProvider->clearCache() || !$this->permissionsListFactory->clearCache()) {
            return false;
        }

        $sessionId = filter_input(INPUT_COOKIE, $this->cookieName);
        $result = $this->authenticator->invalidate($sessionId);
        if (!$result) {
            return false;
        }

        return $this->deleteCookie($this->cookieName);
    }

    /**
     * @return string|false
     */
    public function getCookieTokenValue()
    {
        return filter_input(INPUT_COOKIE, $this->cookieName);
    }

    /*****************************************************************
     * Cookies methods
     *****************************************************************/

    /**
     * @param string $name
     * @param mixed $value
     * @param int $expire
     * @return bool
     */
    protected function setCookie($name, $value, $expire)
    {
        return setcookie($name, $value, $expire, '/', $this->cookieDomain(), false, true);
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function deleteCookie($name)
    {
        return $this->setCookie($name, false, 0);
    }

    /**
     * @return string
     */
    protected function cookieDomain()
    {
        $host = $_SERVER['HTTP_HOST'];
        $parts = array_reverse(explode('.', $host));
        return '.' . $parts[1] . '.' . $parts[0];
    }
}
