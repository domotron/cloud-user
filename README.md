# Cloud user

Domotron Cloud user pre SSO v službách Domotron-u.

### Nastavenie
Základné nastavenie knižnice. Niektoré časti sú závyslé na knižnici cloudClient.

```php
use DomotronCloudUser\IdentityProvider\Cache\SessionCache as IdentitySessionCache;
use DomotronCloudUser\IdentityProvider\ApiIdentityProvider;
use DomotronCloudUser\Authenticator\ApiAuthenticator;
use DomotronCloudUser\Permissions\PermissionsListFactory;
use DomotronCloudUser\Permissions\Cache\SessionCache;
use DomotronCloudUser\CloudUser;
use DomotronCloudUser\Permissions\Driver\ApiDriver;

$authenticator = new ApiAuthenticator($cloudClient);
$identityProvider = new ApiIdentityProvider($cloudClient, new IdentitySessionCache('5 minutes'));
$permissionsListFactory = new PermissionsListFactory(new ApiDriver($cloudClient), new SessionCache('5 minutes'));
$cloudUser = new CloudUser($identityProvider, $authenticator, $permissionsListFactory, 'http://cloud.mydomotron.com');
```

### Použitie
```php
// Overenie či je používateľ prihlásený
$cloudUser->isLoggedIn();

// Ak treba používateľa prihlásiť je potrebné ho poslať na login screen cloudu
// Po úspešnom prihlásení bude použivateľ presmerovaný naspäť na $redirectUri
// $portal a $secret sú identifikátory ktoré sú registrované na Cloude
$cloudUser->getLoginUrl($portal, $secret, $redirectUri);

// Odhlásenie
// Zneplatný token + odstráni cookie. Metóda logout() nerobí žiadne presmerovanie. Ak po odhlásení chceme používateľa presmerovať treba tak urobiť manuálne.
$cloudUser->logout();

// Url pre odhlásenie z cloudu
$cloudUser->getLogoutUrl();

// Data
$identity = $cloudUser->getIdentity(); // Vrácia identitu s dátami používateľa, identitu vráti aj v prípade že používateľ je neautentifikovaný. Táto identita bude ale prázdna.
$identity->getData(); // Vracia pole so všetkými dátami použivateľa

// Data shortcuts
$identity->getName();
$identity->getUsername();
$identity->getId();
$identity->getLanguage();
$identity->isDemo();

// Permissions
// Oprávnenia sú rozdelené na rôzne časti keďže sa jedná o "row level" oprávnenia
// Treba si od cloudu najprv vypítať oprávnenia na to na čo potrebujeme
$permissions = $cloudUser->permissions('control-sk1234');

// Následne sa môžeme pýtať na práva používateľa
$permissions->isAllowed('global', 'access');

// Shortcut pre $cloudUser->permissions($key)->isAllowed($resource, $privilege);
$cloudUser->permissionsIsAllowed($key, $resource, $privilege);

// Zǐskanie tokenu aktuálne prihláseného používateľa
$cloudUser->getCookieTokenValue();
```

### Key alias (permissions)
```php
// Pre zjednodušenie je možné si do permissionsListFactory zaregistrovať key alias
$permissionsListFactory->registerKeyAlias('control', 'control-sk1234');

// Neskôr je možné použiť tento alias pomocou prefixu '%'
$clientUser->permissionsIsAllowed('%control', $resource, $privilege);
```

### Rozšírenia
#### Tracy Bar

Registrácia do Tracy baru v neon súbore:
```php
tracy:
	bar:
		- DomotronCloudUser\Bridge\Tracy\CloudUserPanel
```